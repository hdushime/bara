package com.comza.bara;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

public class DrinkDetails extends AppCompatActivity {
    private TextInputEditText price;
    private TextInputEditText beer_type;
    private TextInputLayout priceLayout;
    private TextInputLayout beerTypeLayout;
    private Button proceed;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drink_details);

        price = findViewById(R.id.p);
        beer_type = findViewById(R.id.t);
        priceLayout = findViewById(R.id.price);
        beerTypeLayout = findViewById(R.id.type);
        proceed = findViewById(R.id.proceed);

        proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (beer_type.getText().toString().isEmpty()) {
                    beerTypeLayout.setError("Please Enter the beer type");
                }
                if (price.getText().toString().isEmpty()) {
                    priceLayout.setError("Please enter the price");
                }
                if (!beer_type.getText().toString().isEmpty() && !price.getText().toString().isEmpty()) {
                    Intent intent = new Intent(DrinkDetails.this, Party.class);
                    String beer = beer_type.getText().toString();
                    String prices = price.getText().toString();
                    intent.putExtra("beer",beer);
                    intent.putExtra("price", prices);
                    startActivity(intent);
                }
            }
        });
    }
}
