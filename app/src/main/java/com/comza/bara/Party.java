package com.comza.bara;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GestureDetectorCompat;

import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.Random;

public class Party extends AppCompatActivity  {
    private TextView count, total, name;
    private ImageButton increase, decrease;
    private Button reset;
    private pl.droidsonroids.gif.GifImageView giff;
    int previous = 0;
    int p = 0;
    int num = 0;
    long timeInMilliseconds = 0L;
    long startTime = 0L;
    String [] giffnames = {
            "b.gif","f.gif","jma.gif"
    };
    private static final String DEBUG_TAG = "Gestures";
    private GestureDetectorCompat mDetector;
    private Handler customHandler = new Handler();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_party);
        String prices = getIntent().getStringExtra("price");
        startTime = SystemClock.uptimeMillis();
        customHandler.postDelayed(updateTimerThread, 0);
        String beerName = getIntent().getStringExtra("beer");
        p = Integer.parseInt(prices);
        count = findViewById(R.id.counter);
        total = findViewById(R.id.price);
        increase = findViewById(R.id.increase);
        decrease = findViewById(R.id.decrease);
        name = findViewById(R.id.beername);
        reset = findViewById(R.id.button2);
        giff = findViewById(R.id.giff);
        Log.v("beer", beerName);
        name.setText(beerName);
        mDetector = new GestureDetectorCompat(this, new MyGestureListener());
        increase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                previous++;
                count.setText((Integer.toString(previous)));
                total.setText(Integer.toString(previous*p));
            }
        });
        decrease.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(previous>0) {
                    previous--;
                    count.setText((Integer.toString(previous)));
                    total.setText(Integer.toString(previous * p));
                }
            }
        });
        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                previous = 0;
                count.setText((Integer.toString(previous)));
                total.setText(Integer.toString(previous*p));
            }
        });

    }
    private Runnable updateTimerThread = new Runnable() {

        public void run() {

            timeInMilliseconds = SystemClock.uptimeMillis() - startTime;
            if (timeInMilliseconds%6000 == 0) {
                int random;
                random = new Random().nextInt(3);
                if (random == 0) {
                    giff.setImageResource(R.drawable.b);
                }
                if (random == 1) {
                    giff.setImageResource(R.drawable.f);
                }
                if (random == 2) {
                    giff.setImageResource(R.drawable.jma);
                }
            }

            customHandler.postDelayed(this, 0);
        }

    };
    @Override
    public boolean onTouchEvent(MotionEvent event){
        this.mDetector.onTouchEvent(event);
        return super.onTouchEvent(event);
    }

    class MyGestureListener extends GestureDetector.SimpleOnGestureListener {
        private static final String DEBUG_TAG = "Gestures";
        @Override
        public boolean onDoubleTap(MotionEvent event) {
            Log.d(DEBUG_TAG, "onDoubleTap: " + event.toString());
            previous++;
            count.setText((Integer.toString(previous)));
            total.setText(Integer.toString(previous*p));
            return true;
        }

        @Override
        public boolean onDoubleTapEvent(MotionEvent event) {
            Log.d(DEBUG_TAG, "onDoubleTapEvent: " + event.toString());
           // previous++;
            //count.setText((Integer.toString(previous)));
            //total.setText(Integer.toString(previous*p));
            return true;
        }

        }

}
